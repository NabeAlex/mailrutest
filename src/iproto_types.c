#include "iproto_types.h"

inline iproto_string_ptr gen_iproto_string(const char *str)
{
    int32_t len = (int32_t) strlen(str);

    iproto_string_ptr data = (iproto_string_ptr) malloc(sizeof(struct iproto_string) + len * sizeof(char));
    if(data) {
        data->str_len =(struct iproto_int32) { len };
        memcpy(data->str, str, len * sizeof(char));
        return data;
    }

    return NULL;
}

inline void free_iproto_string(iproto_string_ptr data) { free(data); }