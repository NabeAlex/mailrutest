#ifndef CUBE_IPROTO_UTIL_H
#define CUBE_IPROTO_UTIL_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

#include "iproto_types.h"

#define SHIFT(prt_value, diff) ((char*)(prt_value) + \
                                (diff))

void print_iproto(FILE *out, iproto_string_ptr str);
void print_line_iproto(FILE *out, iproto_string_ptr str);

typedef struct iproto_container
{
    void *data;
    size_t size_full;
} iproto_container;

iproto_container *init_iproto_container(size_t size_ful);
iproto_container *set_iproto_container(void *data, size_t size_ful);

void remove_iproto_container(iproto_container *container);

void normalize_container(iproto_container *container);

void put_iproto_integer(iproto_container *container, struct iproto_int32 el);
void put_iproto_integer64(iproto_container *container, struct iproto_int64 el);
void put_iproto_string(iproto_container *container, iproto_string_ptr el);

struct iproto_int32 next_iproto_integer(iproto_container *container);
struct iproto_int64 next_iproto_integer64(iproto_container *container);
iproto_string_ptr next_iproto_string(iproto_container *container);

#ifdef DEBUG
void dump_container(iproto_container *container);
void dump_by_ptr(void *data, size_t len);
#endif

#if defined(__cplusplus)
} /* extern "C" */
#endif /* defined(__cplusplus) */

#endif //CUBE_IPROTO_UTIL_H
