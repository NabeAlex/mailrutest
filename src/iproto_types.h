#ifndef CUBE_IPROTO_TYPES_H
#define CUBE_IPROTO_TYPES_H

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

struct iproto_int32
{
    int32_t int32;
};

struct iproto_int64
{
    int64_t int64;
};

struct iproto_string
{
    struct iproto_int32 str_len;
    char str[];
};

typedef struct iproto_string * iproto_string_ptr;

iproto_string_ptr gen_iproto_string(const char *str);
void free_iproto_string(iproto_string_ptr data);

#if defined(__cplusplus)
} /* extern "C" */
#endif /* defined(__cplusplus) */

#endif //CUBE_IPROTO_TYPES_H
