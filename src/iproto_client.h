#ifndef CUBE_IPROTO_CLIENT_H
#define CUBE_IPROTO_CLIENT_H

/*
#define ENABLE_MOCK_SOCKET
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <errno.h>

#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <netdb.h>
#include <stdbool.h>

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

#include "iproto_client_conf.h"
#include "iproto_util.h"

bool set_iproto_socket(const char *host, uint16_t port);

void iproto_disconnect();

/* input data in function is binary (serialize) */
int iproto_send_serialized(void *data, int bytes);

int wait_header_and_return_data(api_header_response *head_data);

int iproto_wait_and_read_serialized(void *input_here, size_t bytes);

#if defined(__cplusplus)
} /* extern "C" */
#endif /* defined(__cplusplus) */

#endif //CUBE_IPROTO_CLIENT_H