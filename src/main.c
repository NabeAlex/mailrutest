#include <stdlib.h>
#include <stdio.h>

#include "client_terminal.h"

void cube_command(int argc, char **argv); /* Declaration of cube function */

int main(int argc, char **argv)
{
    const unsigned int LENGTH_OF_COMMANDS = 1;
    init_terminal(LENGTH_OF_COMMANDS, (struct command_and_func) {"cube", cube_command});

    main_loop(stdin);

    remove_terminal();
    return   EXIT_SUCCESS;
}