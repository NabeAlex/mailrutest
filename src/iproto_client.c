#include "iproto_client.h"

#include <stdbool.h>

int iproto_socket;

/**
 * Exit from recv(...) and send(...) functions
 */
static bool exit_event = false;

/**
 * Callback SIGINT
 * @param sig
 */
void handle_exit(int sig)
{
    exit_event = true;
}

void set_timeout()
{
    struct timeval timeout;
    timeout.tv_sec = TIME_OUT_SEC;
    timeout.tv_usec = 0;

    setsockopt(iproto_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
    setsockopt(iproto_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
}

static inline bool check_sock(int sock_id)
{
    return (sock_id >= 0) ? true : false;
}

static int iproto_connect_with_server(const char *host, uint16_t port)
{
    /* socket
     * dummy protocol for TCP
     */
    iproto_socket = socket(AF_INET, SOCK_STREAM, 0);

    if( !check_sock(iproto_socket) )
    {
        errno = ENOTSOCK;
        return IPROTO_CLIENT_CONNECT_ERROR;
    }

    set_timeout();

    struct sockaddr_in sock_in;
    memset(&sock_in, 0, sizeof sock_in);
    sock_in.sin_family = AF_INET;
    sock_in.sin_addr.s_addr = inet_addr(host);

    printf("Connect %s %d\n", host, port);
    sock_in.sin_port = htons(port);

    if(connect(iproto_socket, (struct sockaddr*) &sock_in, sizeof sock_in) < 0)
    {
        /* Can be errno ~ EINTR */
        return IPROTO_CLIENT_CONNECT_ERROR;
    }

    return IPROTO_CLIENT_CONNECT_OK;
}

bool set_iproto_socket(const char *host, uint16_t port)
{
    signal(SIGINT, handle_exit);
#ifndef ENABLE_MOCK_SOCKET
    struct hostent *he = gethostbyname(host);
    if(!he)
    {
        errno = EHOSTDOWN;
        return false;
    }

    struct in_addr **addr_list;
    addr_list = (struct in_addr **)he->h_addr_list;
    if(addr_list[0] == NULL)
        return false;

    if(iproto_connect_with_server(inet_ntoa(*addr_list[0]), port) != IPROTO_CLIENT_CONNECT_OK)
        return false;
#endif

    return true;
}

void iproto_disconnect() { shutdown(iproto_socket, SHUT_RDWR); }

int iproto_send_serialized(void *data, int bytes) {
    ssize_t sent_data = 0;
    const void *ptr_data = data;

    while(bytes > 0)
    {
        if(exit_event)
            return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR; /* SIGINT */

        sent_data = send(iproto_socket, ptr_data, (size_t) bytes, 0);

        if (sent_data < 0 && errno == EINTR)
            continue;
        else if(sent_data < 0)
            return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR;

        ptr_data = SHIFT(ptr_data, sent_data);
        bytes -= sent_data;
    }

    return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK;
}

int wait_header_and_return_data(api_header_response *head_data)
{
    const unsigned int SIZE_HEADER_RESPONSE = SIZE_API_HEADER_RESPONSE;

    ssize_t read_data = 0;

    if(!head_data)
    {
        errno = ENOMEM;
        return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR;
    }

    /* receive minimum data */
    while(!exit_event) /* SIGINT */
    {
        read_data = recv(iproto_socket, head_data, SIZE_HEADER_RESPONSE, 0);
        if(read_data < 0 && errno == EINTR)
            continue;

        break;
    }

    if(read_data == SIZE_HEADER_RESPONSE)
        return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK;

    errno = EIO;
    return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR;
}

int iproto_wait_and_read_serialized(void *input_here, size_t bytes)
{
    const unsigned int CHUNK_SIZE = 128;

    ssize_t size_read, size_total = 0;
    char chunk[CHUNK_SIZE];

    void *ptr_data = input_here;

    while(size_total < bytes)
    {
        if(exit_event)
            return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR;

        memset(chunk, 0, CHUNK_SIZE); // clear chunk
        if((size_read = recv(iproto_socket, chunk, CHUNK_SIZE, 0) ) < 0)
        {
            if(errno == EINTR)
                continue;

            return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR;
        }
        else if(size_read == 0)
        {
            /* corrupted received data */
            return IPROTO_CLIENT_RECEIVE_SORRY;
        }
        else
        {
            size_total += size_read;

            /* Try to cut rubbish */
            if(size_total > bytes)
            {
                memmove(ptr_data, chunk, (size_t) size_total - bytes);

                while(recv(iproto_socket, chunk, CHUNK_SIZE, 0) <= 0); // fflush in socket
                break;
            }
            memmove(ptr_data, chunk, (size_t) size_read);

            ptr_data = SHIFT(ptr_data, size_read);
        }
    }

    return IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK;
}