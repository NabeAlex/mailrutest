#include "client_terminal.h"

#include <stdbool.h>

static int count_funcers = 0;
static struct command_and_func *funcers = NULL;

base_args parse_argv(char *buf);
void free_base_args(base_args ar);
void execute_shell(exec_func_t func, base_args args);

void init_terminal(int funcs, ...)
{
    va_list arl;
    va_start(arl, funcs);

    funcers = (struct command_and_func*) malloc(funcs * sizeof(struct command_and_func));
    if(funcers == NULL)
    {
        printf("%s", "Cannot allocate memory for funclist");
        return;
    }

    struct command_and_func tmp;
    for(int i = 0; i < funcs; i++)
    {
        tmp = va_arg(arl, struct command_and_func);
        funcers[i] = tmp;
    }
    count_funcers = funcs;
}

void remove_terminal()
{
    free(funcers);
}

void main_loop(FILE *in)
{
    char *buf = NULL;
    size_t data_size;

    bool make_exit = false;
    while(!make_exit)
    {
        printf("%s", "> ");

        if(getline(&buf, &data_size, in) < 0)
        {
            strerror(errno);
            continue;
        }

        base_args ar = parse_argv(buf);

        if(ar.argc <= 0) continue;

        /* IF EXIT */
        if( !strcmp(ar.argv[0], "exit") ) make_exit = true;
        else if(funcers != NULL)
        {
            char *current_command = ar.argv[0];

            /* Linear search by key */
            for(int i = 0; i < count_funcers; i++)
                if(strcmp(funcers[i].data, current_command) == 0)
                    execute_shell(funcers[i].exec_func, ar);

        }

        free(buf); buf = NULL;
        free_base_args(ar);
    }
}

base_args parse_argv(char *buf)
{
    const unsigned int MAX_SPLIT_COMMAND = 64;

    int tmp_argc = 0;
    char **tmp_argv = NULL;

    tmp_argv = (char**) malloc(MAX_SPLIT_COMMAND * sizeof(char*));
    if(!tmp_argv)
    {
        printf("%s", "Cannot allocate memory!\n");
        exit(EXIT_FAILURE);
    }

    const char *SPLIT_STRING = " \r\n";
    char *tok = strtok(buf, SPLIT_STRING);

    while(tok != NULL)
    {
        if(tmp_argc >= MAX_SPLIT_COMMAND)
        {
            printf("%s", "Error! Cannot parse more 64 words!\n");
            exit(EXIT_FAILURE);
        }
        tmp_argv[tmp_argc] = tok;
        tmp_argc++;

        tok = strtok(NULL, SPLIT_STRING);
    }

    return (base_args) { .argc = tmp_argc, .argv = tmp_argv };
}

inline void free_base_args(base_args ar)
{
    free(ar.argv);
}

void execute_shell(exec_func_t func, base_args args) {
    pid_t pid_info;

    int status;

#ifndef DEBUG
    pid_info = fork();
#else
    pid_info = 0;
#endif
    if (pid_info > 0) /* Parent process*/
    {
        do {
            waitpid(pid_info, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    else if(pid_info < 0) /* Func returns error */
    {
        printf("%s", "Error fork()\n");
        return;
    }
    else /* Child func */
    {
        func(args.argc, args.argv);
        exit(EXIT_SUCCESS);
    }
}
