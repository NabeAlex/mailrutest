#ifndef CUBE_CLIENT_TERMINAL_H
#define CUBE_CLIENT_TERMINAL_H

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

typedef void (*exec_func_t)(int argc, char **argv);

typedef struct base_args
{
    int argc;
    char **argv;
} base_args;

struct command_and_func
{
    char *data;
    exec_func_t exec_func;
};

void init_terminal(int funcs, ...);
void remove_terminal();

void main_loop(FILE *in);

#if defined(__cplusplus)
} /* extern "C" */
#endif /* defined(__cplusplus) */

#endif //CUBE_CLIENT_TERMINAL_H
