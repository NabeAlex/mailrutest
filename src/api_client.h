#ifndef CUBE_API_CLIENT_H
#define CUBE_API_CLIENT_H

#include <stdint.h>

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

#include "iproto_types.h"
#include "iproto_util.h"
#include "iproto_client_conf.h"

/*
 * due to these structs we can work with API.
 * This is base Interface and some functions for
 * work with serialize.
 */

#define SIZE_API_SVC_REQUEST(token, scope)  (sizeof(struct iproto_int32)              + \
                                             sizeof(struct iproto_int32) * 2          + \
                                             sizeof(char) * (token)->str_len.int32     + \
                                             sizeof(char) * (scope)->str_len.int32)

/*
 * request.
 * svc_message - basic (id) of method "Check token"
 * token - token
 * scope - unknown :)
 *
 */
typedef struct api_svc_request
{
    struct api_header_request header;
    struct iproto_int32 svc_message;
    iproto_string_ptr token;
    iproto_string_ptr scope;
} api_svc_request;

/* To BINARY */

iproto_container *gen_svc_request_body(api_svc_request *basic);

/* End functions */


/* response.
 * client_id   - id client
 * client_type - type of client
 * username    - user name
 * expires_in  - expire token
 * user_id     - user id
 */
typedef struct api_svc_ok_response
{
    iproto_string_ptr client_id;
    struct iproto_int32 client_type;
    iproto_string_ptr username;
    struct iproto_int32 expires_in;
    struct iproto_int64 user_id;
} api_svc_ok_response;

/* response
 * error_string - string of error
 */
typedef struct api_svc_error_response
{
    iproto_string_ptr error_string;
} api_svc_error_response;

/* from BINARY */

void gen_svc_ok_response_body(iproto_container *container, api_svc_ok_response *tmp);
void remove_svc_ok_response_body(api_svc_ok_response *interface);

void gen_svc_error_response_body(iproto_container *container, api_svc_error_response *tmp);
void remove_svc_error_response_body(api_svc_error_response *interface);

/* end functions */

#if defined(__cplusplus)
} /* extern "C" */
#endif /* defined(__cplusplus) */

#endif //CUBE_API_CLIENT_H
