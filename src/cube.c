#include <stdlib.h>
#include <stdio.h>

#include "client_terminal.h"

#include "api_client.h"
#include "iproto_client.h"

void print_usage_cube()
{
    printf("Usage: [host port token scope] \n");
}

iproto_container *cube_request(void *send_data, size_t send_data_length,
                               struct iproto_int32 *svc_id,
                               struct iproto_int32 *return_code,
                               struct iproto_int32 *request_id) {
    if (iproto_send_serialized(send_data, (int) send_data_length) != IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK) {
        puts(strerror(errno));
        return NULL;
    }

    api_header_response head_res;
    if( wait_header_and_return_data(&head_res) != IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK)
    {
        strerror(errno);
        return NULL;
    }

    size_t body_length = (size_t) head_res.body_length.int32;

    void *body_data = malloc(body_length);
    if(!body_data)
    {
        printf("Cannot allocate memory for body!\n");
        return NULL;
    }

    int stat_listen = iproto_wait_and_read_serialized(body_data, body_length);

    if(stat_listen != IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK)
    {
        if(stat_listen == IPROTO_CLIENT_RECEIVE_SORRY)
            puts("Something go wrong! Package was corrupted.");
        else
            puts(strerror(errno));

        free(body_data);
        return NULL;
    }

    *svc_id = head_res.svc_id;
    *return_code = head_res.return_code;
    *request_id = head_res.request_id;

    return set_iproto_container(body_data, body_length);
}

void resume_of_user(FILE *file, api_svc_ok_response *res)
{
    puts("***********");
    puts("Resume user");
    printf("%s", "username: ");
    print_line_iproto(file, res->username);

    printf("client Type: %d\n", res->client_type.int32);

    printf("%s", "cliend id: ");
    print_line_iproto(file, res->client_id);
}

void cube_free_func() {}

void cube_command(int argc, char **argv)
{
    const char NAME_RUN_PROGRAM[] = "cube";

    if(strcmp(argv[0], NAME_RUN_PROGRAM) != 0)
    {
        printf("%s", "Unknown command\n"
                     "Use <cube> cli\n");
        return;
    }

    /* make sure all the resources are freed at the end */
    // atexit(cube_free_func);

    const unsigned int NORMAL_PARAMETRS = 5;
    if(argc != NORMAL_PARAMETRS) {
        print_usage_cube();
        return;
    }

    const unsigned int INDEX_OF_HOST  = 1;
    const unsigned int INDEX_OF_PORT  = 2;
    const unsigned int INDEX_OF_TOKEN = 3;
    const unsigned int INDEX_OF_SCOPE = 4;

    const char *host = argv[INDEX_OF_HOST];
    const uint16_t port = (uint16_t) atoi(argv[INDEX_OF_PORT]);

    bool flag_connect = set_iproto_socket(host, port);
    if(!flag_connect)
    {
        puts(strerror(errno));
        return;
    }

    const char *token = argv[INDEX_OF_TOKEN];
    const char *scope = argv[INDEX_OF_SCOPE];

    api_svc_request req = { .header = { .svc_id = { 0x00000002 } },
            .svc_message = { 0x00000001 },
            .token = gen_iproto_string(token),
            .scope = gen_iproto_string(scope)
    };

    iproto_container *container_request = gen_svc_request_body(&req);

    struct iproto_int32 svc_id, return_code, request_id;

    iproto_container *response_container = cube_request(container_request->data,
                                                        container_request->size_full,
                                                        &svc_id,
                                                        &return_code,
                                                        &request_id);

    free_iproto_string(req.token);
    free_iproto_string(req.scope);
    remove_iproto_container(container_request);

    if(response_container)
    {
        if(return_code.int32 == CUBE_OAUTH2_ERR_OK)
        {
            api_svc_ok_response *tmp_interface = (api_svc_ok_response*) malloc( sizeof(api_svc_ok_response) );

            gen_svc_ok_response_body(response_container, tmp_interface);
            resume_of_user(stdout, tmp_interface);

            remove_svc_ok_response_body(tmp_interface);
            free(tmp_interface);
        }
        else
        {
            api_svc_error_response *tmp_interface = (api_svc_error_response*) malloc( sizeof(api_svc_error_response) );

            gen_svc_error_response_body(response_container, tmp_interface);
            print_line_iproto(stdout, tmp_interface->error_string);

            remove_svc_error_response_body(tmp_interface);
            free(tmp_interface);
        }

        remove_iproto_container(response_container);
    }
    else
        printf("%s", "No response from server!\n");

    iproto_disconnect();
}