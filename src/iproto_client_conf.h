#ifndef CUBE_IPROTO_CLIENT_CONF_H
#define CUBE_IPROTO_CLIENT_CONF_H

#include "iproto_types.h"

/*
 * settings
 * tIME_OUT_SEC - socket option to set timeouts
 */

#define TIME_OUT_SEC 3

/* return about socket(...) and connect(...) */
#define IPROTO_CLIENT_CONNECT_OK           0
#define IPROTO_CLIENT_CONNECT_ERROR        1

/* return if read or write */
#define IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_OK    0
#define IPROTO_CLIENT_RECEIVE_OR_SEND_DATA_ERROR 1
#define IPROTO_CLIENT_RECEIVE_SORRY              2

/* table RETURN_CODE */

#define CUBE_OAUTH2_ERR_OK                  0x00000000
#define CUBE_OAUTH2_ERR_TOKEN_NOT_FOUND     0x00000001
#define CUBE_OAUTH2_ERR_DB_ERROR            0x00000002
#define CUBE_OAUTH2_ERR_UNKNOWN_MSG         0x00000003
#define CUBE_OAUTH2_ERR_BAD_PACKET          0x00000004
#define CUBE_OAUTH2_ERR_BAD_CLIENT          0x00000005
#define CUBE_OAUTH2_ERR_BAD_SCOPE           0x00000006

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */

/* request header */
typedef struct api_header_request
{
    struct iproto_int32 svc_id;
    struct iproto_int32 body_length; // Automatic
} api_header_request;

#define SIZE_API_HEADER_REQUEST sizeof(api_header_request)

/* response header
 * request_id - id of request
 */
typedef struct api_header_response
{
    struct iproto_int32 svc_id;
    struct iproto_int32 body_length;
    struct iproto_int32 request_id;

    /* I moved return_code in header */
    struct iproto_int32 return_code;
} api_header_response;

#define SIZE_API_HEADER_RESPONSE sizeof(api_header_response)

#if defined(__cplusplus)
} /* extern "C" */
#endif /* defined(__cplusplus) */

#endif //CUBE_IPROTO_CLIENT_CONF_H

