#include "iproto_util.h"

#define ALWAYS_INLINE __attribute__((always_inline)) inline

iproto_container *init_iproto_container(size_t size_ful)
{
    iproto_container *container = (iproto_container*) malloc(sizeof(iproto_container));
    if(container)
    {
        container->data = malloc(size_ful);
        if(!container->data)
        {
            free(container);
            return NULL;
        }
        container->size_full = size_ful;
        return container;
    }

    return NULL;
}

iproto_container *set_iproto_container(void *data, size_t size_ful)
{
    iproto_container *container = (iproto_container*) malloc( sizeof(iproto_container) );
    if(container)
    {
        container->data = data;
        container->size_full = size_ful;
        return container;
    }
    return NULL;
}

void remove_iproto_container(iproto_container *container)
{
    free(container->data);
    free(container);
}

ALWAYS_INLINE void normalize_container(iproto_container *container) { container->data = SHIFT(container->data,
                                                                                       -container->size_full); }

inline void put_iproto_integer(iproto_container *container, struct iproto_int32 el)
{
    memcpy(container->data, &el, sizeof(struct iproto_int32));
    container->data = SHIFT(container->data, sizeof(struct iproto_int32));
}

void put_iproto_integer64(iproto_container *container, struct iproto_int64 el)
{
    memcpy(container->data, &el, sizeof(struct iproto_int64));
    container->data = SHIFT(container->data, sizeof(struct iproto_int64));
}

inline void put_iproto_string(iproto_container *container, iproto_string_ptr el)
{
    put_iproto_integer(container, el->str_len);

    memcpy(container->data, el->str, el->str_len.int32 * sizeof(char));
    container->data = SHIFT(container->data, el->str_len.int32 * sizeof(char));
}

struct iproto_int32 next_iproto_integer(iproto_container *container)
{
    struct iproto_int32 temp;
    memmove(&temp, container->data, sizeof(struct iproto_int32));
    container->data = SHIFT(container->data, sizeof(struct iproto_int32));
    return temp;
}

struct iproto_int64 next_iproto_integer64(iproto_container *container)
{
    struct iproto_int64 temp;
    memmove(&temp,  container->data, sizeof(struct iproto_int64));
    container->data = SHIFT(container->data, sizeof(struct iproto_int64));
    return temp;
}

iproto_string_ptr next_iproto_string(iproto_container *container)
{
    struct iproto_int32 length = next_iproto_integer(container);

    const unsigned BYTES_OF_LENGTH = length.int32 * sizeof(char);
    iproto_string_ptr temp = (iproto_string_ptr) malloc(sizeof(struct iproto_string) +
                                                                BYTES_OF_LENGTH);
    temp->str_len = length;
    memmove(temp->str, container->data, BYTES_OF_LENGTH);
    container->data = SHIFT(container->data, BYTES_OF_LENGTH);
    return temp;
}

void print_line_iproto(FILE *out, iproto_string_ptr str)
{
    print_iproto(out, str);
    puts("");
}

void print_iproto(FILE *out, iproto_string_ptr str)
{
    for(int i = 0; i < str->str_len.int32; i++)
        fputc(str->str[i], out);
}

#ifdef DEBUG

void dump_container(iproto_container *container)
{
    printf("Map container:\n");
    for(int i = 0; i < container->size_full; i++)
    {
        printf("%x ", ((char*) container->data)[i] & 0xff);
    }
    printf("\n");
}

void dump_by_ptr(void *data, size_t len)
{
    iproto_container ic = {.data = data, .size_full = len};
    dump_container(&ic);
}

#endif