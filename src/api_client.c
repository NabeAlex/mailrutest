#include "api_client.h"

iproto_container *gen_svc_request_body(api_svc_request *basic)
{
    const size_t length_buf_body = SIZE_API_SVC_REQUEST(basic->token, basic->scope);

    iproto_container *container = init_iproto_container(SIZE_API_HEADER_REQUEST + length_buf_body);

    put_iproto_integer(container, basic->header.svc_id);
    put_iproto_integer(container, (struct iproto_int32) {(int32_t) length_buf_body});

    put_iproto_integer(container, basic->svc_message);
    put_iproto_string(container, basic->token);
    put_iproto_string(container, basic->scope);

    normalize_container(container);

    return container;
}

void gen_svc_ok_response_body(iproto_container *container, api_svc_ok_response *tmp)
{
    if(!tmp)
        return;

    tmp->client_id = next_iproto_string(container);
    tmp->client_type = next_iproto_integer(container);
    tmp->username = next_iproto_string(container);
    tmp->expires_in = next_iproto_integer(container);
    tmp->user_id = next_iproto_integer64(container);

    normalize_container(container);
}

void gen_svc_error_response_body(iproto_container *container, api_svc_error_response *tmp)
{
    if(!tmp)
        return;

    tmp->error_string = next_iproto_string(container);

    normalize_container(container);
}

void remove_svc_ok_response_body(api_svc_ok_response *interface)
{
    free_iproto_string(interface->client_id);
    free_iproto_string(interface->username);
}

void remove_svc_error_response_body(api_svc_error_response *interface)
{
    free_iproto_string(interface->error_string);
}