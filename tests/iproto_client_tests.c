#define ENABLE_MOCK_SOCKET

#include "../src/iproto_client.h"

extern int iproto_socket;

void cube_command(int argc, char **argv);

const unsigned int SIZE_HEADER_BODY = 16;
const unsigned int SIZE_MOCK_OK_RESPONSE_BODY = 54;
const unsigned int SIZE_MOCK_ERROR_RESPONSE_BODY = 13;

void add_header(iproto_container *tmp, int32_t svc_id, int32_t length, int32_t request_id, int32_t return_code)
{
    put_iproto_integer(tmp, (struct iproto_int32) { svc_id });
    put_iproto_integer(tmp, (struct iproto_int32) { length });          /* length */
    put_iproto_integer(tmp, (struct iproto_int32) { request_id });      /* test data! */

    put_iproto_integer(tmp, (struct iproto_int32) { return_code });
}


iproto_container *mock_ok_response()
{
    int32_t body_length = SIZE_MOCK_OK_RESPONSE_BODY;

    iproto_container *tmp = init_iproto_container(SIZE_HEADER_BODY + SIZE_MOCK_OK_RESPONSE_BODY);
    iproto_string_ptr ptr_string;

    add_header(tmp, 2, body_length, 2, IPROTO_CLIENT_CONNECT_OK);

    put_iproto_string(tmp, ptr_string = gen_iproto_string("test_client_id")); /* client_id: test_client_id */
    free_iproto_string(ptr_string);

    put_iproto_integer(tmp, (struct iproto_int32) {2002}); /* client_type: 2002 */

    put_iproto_string(tmp, ptr_string = gen_iproto_string("testuser@mail.ru")); /* username: testuser@mail.ru */
    free_iproto_string(ptr_string);

    put_iproto_integer(tmp, (struct iproto_int32) {3600}); /* expires_in: 3600 */
    put_iproto_integer64(tmp, (struct iproto_int64) {101010}); /* user_id: 101010 */

    normalize_container(tmp);
    return tmp;
}

iproto_container *mock_error_response()
{
    int32_t body_length = SIZE_MOCK_ERROR_RESPONSE_BODY;

    iproto_container *tmp = init_iproto_container(SIZE_HEADER_BODY + SIZE_MOCK_ERROR_RESPONSE_BODY);

    add_header(tmp, 2, body_length, 2, CUBE_OAUTH2_ERR_BAD_SCOPE);
    put_iproto_string(tmp, gen_iproto_string("bad scope"));

    normalize_container(tmp);

    return tmp;
}

int main(int argc, char **argv)
{
    /* choose data */
    printf("%s\n", "Choose method ((o)k / (e)rror)");
    char choose_method;
    scanf("%c", &choose_method);
    /***************/

    int fd[2];
    socketpair(PF_LOCAL, SOCK_STREAM, 0, fd);

    iproto_socket = fd[0]; /* direct set variable */
    int base_socket = fd[1];

    pid_t pid_info;
    pid_info = fork();

    if(pid_info != 0)
    {
        close(base_socket);

        char *cube_argv[] = {"cube", "cube.testserver.mail.ru", "4995", "abracadabra", "test"};
        cube_command(5, cube_argv);
    }
    else
    {
        close(iproto_socket);

        size_t SIZE_MOCK_SEND_DATA = 35;
        iproto_container *request_space = init_iproto_container(SIZE_MOCK_SEND_DATA);
        recv(base_socket, request_space->data, SIZE_MOCK_SEND_DATA, 0);

        iproto_container *current_container = (choose_method == 'o') ? mock_ok_response() : mock_error_response();
        send(base_socket, current_container->data, current_container->size_full, 0);

        remove_iproto_container(request_space);
        remove_iproto_container(current_container);
        close(base_socket);
    }
    return EXIT_SUCCESS;
}